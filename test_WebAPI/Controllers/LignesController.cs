﻿using ApiDB.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using test_WebAPI.Models;

namespace test_WebAPI.Controllers
{
    public class LignesController : ApiController
    {
        public IEnumerable<Lignes_Commandes> Get(int id)
        {
            Repo_Lignes repo = new Repo_Lignes();
            IEnumerable<Lignes_Commandes> mesprod = repo.Get().Select(ApiP => new Lignes_Commandes() { Id = ApiP.Id, Commandes_ID = ApiP.Commandes_ID, prix = ApiP.prix,Produits_Id = ApiP.Produits_Id,quantitee=ApiP.quantitee });
            return mesprod;
        }



        public void update(Lignes_Commandes p)
        {
            Repo_Lignes repo = new Repo_Lignes();

            ApiDB.Model.Lignes_commandes ApiP = new ApiDB.Model.Lignes_commandes();
            ApiP.Id = p.Id;
            ApiP.prix = p.prix;
            ApiP.Commandes_ID = p.Commandes_ID;
            ApiP.Produits_Id = p.Produits_Id;
            ApiP.quantitee = p.quantitee;
            repo.Update(ApiP);

        }
        public void Insert(Lignes_Commandes p)
        {
            Repo_Lignes repo = new Repo_Lignes();
            ApiDB.Model.Lignes_commandes apiP = new ApiDB.Model.Lignes_commandes();

            apiP.Id = p.Id;
            apiP.prix = p.prix;
            apiP.Commandes_ID = p.Commandes_ID;
            apiP.Produits_Id = p.Produits_Id;
            apiP.quantitee = p.quantitee;

            repo.Insert(apiP);
        }
    }
}
