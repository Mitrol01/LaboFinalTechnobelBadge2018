﻿using ApiDB.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using test_WebAPI.Models;
using ToolBox.Patterns.Repository;

namespace test_WebAPI.Controllers
{
    public class CategorieeController : ApiController
    {
        public IEnumerable<Categories> Get()
        {
            Repository repo = new Repository();
            IEnumerable<Categories> mesprod = repo.Get().Select(ApiP => new Categories() { ID = ApiP.ID, Name = ApiP.Name  });
            return mesprod;
        }

        public Categories Get(int ID)
        {
            Repository repo = new Repository();
            Categories mesprod = repo.Get().Select(ApiP => new Categories() { ID = ApiP.ID, Name = ApiP.Name }).SingleOrDefault(p=> p.ID==ID);
            return mesprod;
        }

        public void update(Categories p)
        {
            Repository repo = new Repository();

            ApiDB.Model.Categorie ApiP = new ApiDB.Model.Categorie();
            ApiP.ID = p.ID;
            ApiP.Name = p.Name;
            
            repo.Update(ApiP);

        }
        public void Insert(Categories p)
        {
            Repository repo = new Repository();
            ApiDB.Model.Categorie apiP = new ApiDB.Model.Categorie();

            apiP.ID = p.ID;
            apiP.Name = p.Name;

            repo.Insert(apiP);
        }
    }
}
