﻿using ApiDB.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using test_WebAPI.Models;

namespace test_WebAPI.Controllers
{
    public class CommandesController : ApiController
    {
        public IEnumerable<Commandes> Get(int id)
        {
            Repo_Commandes repo = new Repo_Commandes();
            IEnumerable<Commandes> mesprod = repo.Get().Select(ApiP => new Commandes() { ID = ApiP.ID, Clients = ApiP.Clients, Produits_ID = ApiP.Produits_ID,Temps = ApiP.Temps});
            return mesprod;
        }



        public void update(Commandes p)
        {
            Repo_Commandes repo = new Repo_Commandes();

            ApiDB.Model.Commandes ApiP = new ApiDB.Model.Commandes();
            ApiP.ID = p.ID;
            ApiP.Clients = p.Clients;
            ApiP.Produits_ID = p.Produits_ID;
            ApiP.Temps = p.Temps;
            repo.Update(ApiP);

        }
        public void Insert(Commandes p)
        {
            Repo_Commandes repo = new Repo_Commandes();
            ApiDB.Model.Commandes apiP = new ApiDB.Model.Commandes();

            apiP.ID = p.ID;
            apiP.Clients = p.Clients;
            apiP.Produits_ID = p.Produits_ID;
            apiP.Temps = p.Temps;
            repo.Insert(apiP);
        }
    }
}
