﻿using ApiDB.Repositories;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using test_WebAPI.Models;

namespace test_WebAPI.Controllers
{
    public class ProductController : ApiController
    {
        public IEnumerable<produit> Get()
        {
            Repo_Produits repo = new Repo_Produits();
            IEnumerable<produit> mesprod = repo.Get().Select(ApiP => new produit() { ID = ApiP.ID, Nom = ApiP.Name, Prix = ApiP.prix });
            return mesprod;
        }
        public IEnumerable<produit> Get(int id)
        {
            Repo_Produits repo = new Repo_Produits();
            IEnumerable<produit> mesprod = repo.Get().Where(C => C.Categorie_ID==id).Select(ApiP => new produit() { ID = ApiP.ID, Nom = ApiP.Name,Prix=ApiP.prix,Categorie_ID= ApiP.Categorie_ID });
            return mesprod;
        }



        public void update(produit p)
        {
            Repo_Produits repo = new Repo_Produits();

            ApiDB.Model.Produits ApiP = new ApiDB.Model.Produits();
            ApiP.ID = p.ID;
            ApiP.Name = p.Nom;
            ApiP.prix = p.Prix;
            repo.Update(ApiP);

        }
        public void Insert(produit p)
        {
            Repo_Produits repo = new Repo_Produits();
            ApiDB.Model.Produits apiP = new ApiDB.Model.Produits();

            apiP.ID = p.ID;
            apiP.Name = p.Nom;
            apiP.prix = p.Prix;

            repo.Insert(apiP);
        }
    }
    
}
