﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace test_WebAPI.Models
{
	public class produit
	{
		public int ID { get; set; }
		public string Nom { get; set; }
		public decimal Prix { get; set; }
        public int Categorie_ID { get; set; }
    }
}