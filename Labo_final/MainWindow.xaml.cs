﻿using Labo_final.Models;
using Labo_final.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Labo_final
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new VewModel_Categorie();
           
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ((VewModel_Categorie)DataContext).LoadeProduits(1);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ((VewModel_Categorie)DataContext).LoadeProduits(2);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            ((VewModel_Categorie)DataContext).LoadeProduits(3);
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            ((VewModel_Categorie)DataContext).LoadeProduits(4);
        }
    }
}
