﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labo_final.Models
{
    class Commandes
    {
        public int ID { get; set; }
        public int Produits_ID { get; set; }
        public bool Clients { get; set; }
        public DateTime Temps { get; set; }
    }
}
