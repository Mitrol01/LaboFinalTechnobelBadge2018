﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Labo_final.Models
{
   public class Produits
    {
        public int ID { get; set; }
        [JsonProperty("Nom")]
        public string Name { get; set; }
        public decimal prix { get; set; }
        public int Categorie_ID { get; set; }
    }
}
