﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labo_final.Models
{
    public class Categories
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
