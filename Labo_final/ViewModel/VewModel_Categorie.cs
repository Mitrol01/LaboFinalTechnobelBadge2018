﻿using Labo_final.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Labo_final.ViewModel
{
    class VewModel_Categorie
    {
        public Categories cat1 { get; set; }
        public Categories cat2 { get; set; }
        public Categories cat3 { get; set; }
        public Categories cat4 { get; set; }
        public ObservableCollection<Produits> ListeProduits { get; set; }
        private HttpClient client;
        private HttpRequestMessage request;

        public VewModel_Categorie()
        {
            cat1 = GetCategories(1);
            cat2 = GetCategories(2);
            cat3 = GetCategories(3);
            cat4 = GetCategories(4);
            ListeProduits = new ObservableCollection<Models.Produits>();
        }

        private Categories GetCategories(int id)
        {
            Categories c = instanciate<Categories>("http://localhost:50239/api/Categoriee/" + id);
            return c;
        }
        

        public void LoadeProduits(int catid)
        {
           List<Produits> c=instanciate<List<Produits>>("http://localhost:50239/api/Product/" + catid);
            ListeProduits.Clear();
            foreach (var item in c)
            {
                ListeProduits.Add(item);
            }
            
        }
        public T instanciate<T>(string url)
        {
            client = new HttpClient();
            request = new HttpRequestMessage();
            var result = client.GetAsync(new Uri(url), HttpCompletionOption.ResponseContentRead).Result;
            string jsonstring = result.Content.ReadAsStringAsync().Result;
            T c = JsonConvert.DeserializeObject<T>(jsonstring);
            return c;
        }
        
    }
}
