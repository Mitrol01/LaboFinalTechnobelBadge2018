﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiDB.Model
{
    public class Produits
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public decimal prix { get; set; }
        public int Categorie_ID { get; set; }
    }
}
