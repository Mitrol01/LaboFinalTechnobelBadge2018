﻿using ApiDB.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.DataAccess.DataBase;

namespace ApiDB.Repositories
{
    public class Repo_Commandes
    {
        private connection connection;
        private const string constr = @"Server=TECHNOBEL\MSSQL2014; Database=Labo_Final;Trusted_Connection=True;";

        public Repo_Commandes()
        {
           
            connection = new connection("System.Data.SqlClient", constr);
        }
        public IEnumerable<Commandes> Get()
        {
            Command Select = new Command("Select ID, Name,prix,Catégorie_ID from Produits;");
            return connection.ExecuteReader(Select, (dr) => new Commandes() { ID = (int)dr["ID"], Clients = (bool)dr["Clients"], Produits_ID = (int)dr["Produits_ID"], Temps = (DateTime)dr["Heure"] });
        }

        public Commandes Get(int ID)
        {
            Command Select = new Command("Select ID, Name,prix,Catégorie_ID from Produits where ID = @ID;");
            Select.AddParameter("ID", ID);
            return connection.ExecuteReader(Select, (dr) => new Commandes() { ID = (int)dr["ID"], Clients = (bool)dr["Clients"], Produits_ID = (int)dr["Produits_ID"], Temps = (DateTime)dr["Heure"] }).FirstOrDefault();
        }

        public void Insert(Commandes Produits)
        {
            Command insert = new Command("insert into Produits output inserted.ID (ID,Client,Produits_ID,Temps) Values (@Id,@Client,@Produits_ID,@Temps ");

            insert.AddParameter("ID", Produits.ID);
            insert.AddParameter("Client", Produits.Clients);
            insert.AddParameter("Produits_ID", Produits.Produits_ID);
            insert.AddParameter("Temps", Produits.Temps);

            Produits.ID = (int)connection.ExecuteScalar(insert);
        }

        public void Update(Commandes Produits)
        {
            Command update = new Command("update Produits set Client = @Client, Produits_ID = @Produits_ID,Temps= @Temps where  ID = @ID ", true);
            //parameters
            update.AddParameter("ID", Produits.ID);
            update.AddParameter("Client", Produits.Clients);
            update.AddParameter("Produits_ID", Produits.Produits_ID);
            update.AddParameter("Temps", Produits.Temps);

            int row = (int)connection.ExecuteNonQuery(update);
        }
        public void Delete(int ID)
        {
            throw new NotImplementedException();
        }
    }
}
