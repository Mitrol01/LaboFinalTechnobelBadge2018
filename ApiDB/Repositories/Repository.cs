﻿using ApiDB.Model;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.DataAccess.DataBase;
using ToolBox.Patterns.Repository;

namespace ApiDB.Repositories
{
  public  class Repository : IRepository<int, Categorie>
    {
        private connection connection;
        private const string constr= @"Server=TECHNOBEL\MSSQL2014; Database=Labo_Final;Trusted_Connection=True;";
        public Repository()
        {
           
            connection = new connection("System.Data.SqlClient", constr);
        }



        public IEnumerable<Categorie> Get()
        {
            Command Select = new Command("Select ID, Name from Catégorie;");
            return connection.ExecuteReader(Select, (dr) => new Categorie() { ID = (int)dr["ID"], Name = (string)dr["Name"] });
        }

        public Categorie Get(int ID)
        {
            Command Select = new Command("Select ID,Name from Catégorie where ID = @ID;");
            Select.AddParameter("ID", ID);
            return connection.ExecuteReader(Select, (dr) => new Categorie() { ID = (int)dr["ID"], Name = (string)dr["Name"] }).SingleOrDefault();
        }

        public void Insert(Categorie categorie)
        {
            Command insert = new Command("insert into Catégorie output inserted.ID (ID,Name) values (@ID,@Name)");

            //parameters

            insert.AddParameter("ID", categorie.ID);
            insert.AddParameter("Name", categorie.Name);

            categorie.ID = (int) connection.ExecuteScalar(insert);
            
            
        }

        public void Update(Categorie categorie)
        {
            Command update = new Command("update Catégorie set Name = @Name where  ID = @ID ", true);
            //parameters
            update.AddParameter("ID", categorie.ID);
            update.AddParameter("Name", categorie.Name);

            int row = (int)connection.ExecuteNonQuery(update);
        }
        public void Delete(int ID)
        {
          throw new NotImplementedException();
        }
    }
}
