﻿using ApiDB.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.DataAccess.DataBase;

namespace ApiDB.Repositories
{
    public class Repo_Produits
    {
        private connection connection;
        private const string constr = @"Server=TECHNOBEL\MSSQL2014; Database=Labo_Final;Trusted_Connection=True;";

        public Repo_Produits( )
        {
           
            connection = new connection("System.Data.SqlClient",constr );
        }
        public IEnumerable<Produits> Get()
        {
            Command Select = new Command("Select ID, Name,prix,Catégorie_ID from Produits;");
            return connection.ExecuteReader(Select, (dr) => new Produits() { ID = (int)dr["ID"], Name = (string)dr["Name"], prix= (decimal)dr["Prix"], Categorie_ID= (int)dr["catégorie_ID"] });
        }

        public Produits Get(int ID)
        {
            Command Select = new Command("Select ID, Name,prix,Catégorie_ID from Produits where ID = @ID;");
            Select.AddParameter("ID", ID);
            return connection.ExecuteReader(Select, (dr) => new Produits() { ID = (int)dr["ID"], Name = (string)dr["Name"], prix = (decimal)dr["Prix"], Categorie_ID = (int)dr["catégorie_ID"] }).FirstOrDefault();
        }

        public void Insert(Produits Produits)
        {
            Command insert = new Command("insert into Produits output inserted.ID (ID,Name,Prix,Categorie_ID) Values (@Id,@Name,@Prix,@Catégorie_ID ");

            insert.AddParameter("ID",Produits.ID );
            insert.AddParameter("Name", Produits.Name );
            insert.AddParameter("Prix", Produits.prix );
            insert.AddParameter("Catégorie_ID", Produits.Categorie_ID);

            Produits.ID = (int)connection.ExecuteScalar(insert);
        }

        public void Update(Produits produits)
        {
            Command update = new Command("update Produits set Name = @Name, Prix = @Prix,Categorie_ID= @Categorie_ID where  ID = @ID ", true);
            //parameters
            update.AddParameter("ID", produits.ID);
            update.AddParameter("Name", produits.Name);
            update.AddParameter("Prix", produits.prix);
            update.AddParameter("Categorie_ID", produits.Categorie_ID);

            int row = (int)connection.ExecuteNonQuery(update);
        }
        public void Delete(int ID)
        {
            throw new NotImplementedException();
        }
    }
}
