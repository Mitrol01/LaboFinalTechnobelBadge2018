﻿using ApiDB.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.DataAccess.DataBase;

namespace ApiDB.Repositories
{
   public class Repo_Lignes
    {
        private connection connection;
        private const string constr = @"Server=TECHNOBEL\MSSQL2014; Database=Labo_Final;Trusted_Connection=True;";

        public Repo_Lignes()
        {
            
            connection = new connection("System.Data.SqlClient", constr);
        }
        public IEnumerable<Lignes_commandes> Get()
        {
            Command Select = new Command("Select ID, Name,prix,Catégorie_ID from Produits;");
            return connection.ExecuteReader(Select, (dr) => new Lignes_commandes() { Id = (int)dr["ID"], Commandes_ID = (int)dr["Commandes_ID"], prix = (int)dr["Prix"], Produits_Id = (int)dr["Produits_ID"],quantitee= (int)dr["Quantitee"] });
        }

        public Lignes_commandes Get(int ID)
        {
            Command Select = new Command("Select ID, Name,prix,Catégorie_ID from Produits where ID = @ID;");
            Select.AddParameter("ID", ID);
            return connection.ExecuteReader(Select, (dr) => new Lignes_commandes() { Id = (int)dr["ID"], Commandes_ID = (int)dr["Commandes_ID"], prix = (int)dr["Prix"], Produits_Id = (int)dr["Produits_ID"],quantitee = (int)dr["Quantitee"] }).FirstOrDefault();
        }

        public void Insert(Lignes_commandes Lignes)
        {
            Command insert = new Command("insert into Produits output inserted.ID (ID,Commandes_ID,Prix,Produits_ID,Quantitee) Values (@Id,@Commandes_ID,@Prix,@Produits_ID,@Quantitee ");

            insert.AddParameter("ID", Lignes.Id);
            insert.AddParameter("Commandes_ID", Lignes.Commandes_ID);
            insert.AddParameter("Prix", Lignes.prix);
            insert.AddParameter("Produits_ID", Lignes.Produits_Id);
            insert.AddParameter("Quantitee", Lignes.quantitee);


            Lignes.Id = (int)connection.ExecuteScalar(insert);
        }

        public void Update(Lignes_commandes Lignes)
        {
            Command update = new Command("update Produits set Commandes_ID = @Commande_ID, Prix = @Prix,Produits_ID= @Produits_ID,Quantitee=@Quantitee where  ID = @ID ", true);
            //parameters
            update.AddParameter("ID", Lignes.Id);
            update.AddParameter("Commandes_ID", Lignes.Commandes_ID);
            update.AddParameter("Prix", Lignes.prix);
            update.AddParameter("Produits_ID", Lignes.Produits_Id);
            update.AddParameter("Quantitee", Lignes.quantitee);

            int row = (int)connection.ExecuteNonQuery(update);
        }
        public void Delete(int ID)
        {
            throw new NotImplementedException();
        }
    }
}
